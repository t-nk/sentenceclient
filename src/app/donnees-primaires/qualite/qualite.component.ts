import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-qualite',
  templateUrl: './qualite.component.html',
  styleUrls: ['./qualite.component.css']
})
export class QualiteComponent implements OnInit {

  qualites: any;
  qualite: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'QUA';
  

  constructor(private generalService: GeneralService
    ,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetQualitesByPages();
  }

  onGetQualitesByPages(){
    this.generalService.getPagination(API_URL.QualitesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.qualites = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageQualite(i:number){
    this.current_page = i;
    this.onGetQualitesByPages;
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchQualite();
  }

  SearchQualite(){
    this.generalService.getByKeyword(API_URL.QualitesSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.qualites = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveQualite(form:any){
    this.generalService.saveResource(API_URL.QualitesUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetQualitesByPages()
    },err=>{
      console.log(err);
    })
  }

  onEdit(q:any){
    this.update = 1;
    this.qualite = q;
  }

  onUpdateQualite(values:any){
    this.generalService.updateResource(API_URL.QualitesUrl+"/"+this.qualite.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetQualitesByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}