import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-pays',
  templateUrl: './pays.component.html',
  styleUrls: ['./pays.component.css']
})
export class PaysComponent implements OnInit {

  payses: any;
  pays: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'PAY';
  

  constructor(private generalService: GeneralService
    ,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetPaysesByPages();
  }

  onGetPaysesByPages(){
    this.generalService.getPagination(API_URL.PaysUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.payses = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPagePays(i:number){
    this.current_page = i;
    this.onGetPaysesByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchPays();
  }

  SearchPays(){
    this.generalService.getByKeyword(API_URL.PaysSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.payses = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSavePays(form:any){
    this.generalService.saveResource(API_URL.PaysUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetPaysesByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(p:any){
    this.update = 1;
    this.pays = p;
  }

  onUpdatePays(values:any){
    this.generalService.updateResource(API_URL.PaysUrl+"/"+this.pays.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetPaysesByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

}
