import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { GeneralService } from '../../services/general.service';
import {API_URL} from '../../config/api.url.config';
import {AuthService} from '../../services/auth.service' 

@Component({
  selector: 'app-maison-arret',
  templateUrl: './maison-arret.component.html',
  styleUrls: ['./maison-arret.component.css']
})
export class MaisonArretComponent implements OnInit {

  maisonArrets: any;
  maisonArret: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0; 
  tableCode: string = 'M-A';
  

  constructor(private generalService: GeneralService, 
    private authService: AuthService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetMaisonArretsByPages();
  }

  onGetMaisonArretsByPages(){
    this.generalService.getPagination(API_URL.MaisonArretUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.maisonArrets = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageMaisonArret(i:number){
    this.current_page = i;
    this.onGetMaisonArretsByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchMaisonArret();
  }

  SearchMaisonArret(){
    this.generalService.getByKeyword(API_URL.MaisonArretSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.maisonArrets = data;
    },error=>{
      console.log(error);
    })
  }

  onSaveMaisonArret(form:any){
    this.generalService.saveResource(API_URL.MaisonArretUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetMaisonArretsByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(m:any){
    this.update = 1;
    this.maisonArret = m;
  }

  onUpdateMaisonArret(values:any){
    this.generalService.updateResource(API_URL.MaisonArretUrl+"/"+this.maisonArret.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetMaisonArretsByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
