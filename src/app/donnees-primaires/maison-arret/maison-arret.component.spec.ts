import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaisonArretComponent } from './maison-arret.component';

describe('MaisonArretComponent', () => {
  let component: MaisonArretComponent;
  let fixture: ComponentFixture<MaisonArretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaisonArretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaisonArretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
