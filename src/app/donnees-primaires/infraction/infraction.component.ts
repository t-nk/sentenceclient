import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-infraction',
  templateUrl: './infraction.component.html',
  styleUrls: ['./infraction.component.css']
})

export class InfractionComponent implements OnInit {
  infractions: any;
  infraction: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'INF';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetInfractionsByPages();
  }

  onGetInfractionsByPages(){
    this.generalService.getPagination(API_URL.InfractionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages= new Array<number>(this.totalPages);
      this.infractions = data;
    },error=>{
      console.log(error);
    })
  }

  onPageInfraction(i:number){
    this.current_page = i;
    this.onGetInfractionsByPages;
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchInfraction();
  }

  SearchInfraction(){
    this.generalService.getByKeyword(API_URL.InfractionsSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.infractions = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveInfraction(form:any){
    this.generalService.saveResource(API_URL.InfractionsUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetInfractionsByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(i:any){
    this.update = 1;
    this.infraction = i;
  }

  onUpdateInfraction(values:any){
    this.generalService.updateResource(API_URL.InfractionsUrl+"/"+this.infraction.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetInfractionsByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
