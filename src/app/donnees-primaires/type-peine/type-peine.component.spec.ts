import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypePeineComponent } from './type-peine.component';

describe('TypePeineComponent', () => {
  let component: TypePeineComponent;
  let fixture: ComponentFixture<TypePeineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypePeineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypePeineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
