import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-type-peine',
  templateUrl: './type-peine.component.html',
  styleUrls: ['./type-peine.component.css']
})
export class TypePeineComponent implements OnInit {

  typePeines: any;
  typePeine: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'T-PE';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetTypesPeineByPages();
  }

  onGetTypesPeineByPages(){
    this.generalService.getPagination(API_URL.TypesPeineUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages=new Array<number>(this.totalPages);
      this.typePeines = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchPeine();
  }

  SearchPeine(){
    this.generalService.getByKeyword(API_URL.TypesPeineSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.typePeines = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveTypesPeine(form:any){
    this.generalService.saveResource(API_URL.TypesPeineUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetTypesPeineByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(tP:any){
    this.update = 1;
    this.typePeine = tP;
  }

  onUpdateTypesPeine(values:any){
    this.generalService.updateResource(API_URL.TypesPeineUrl+"/"+this.typePeine.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetTypesPeineByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
