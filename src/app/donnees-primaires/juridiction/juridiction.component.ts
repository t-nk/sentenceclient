import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-juridiction',
  templateUrl: './juridiction.component.html',
  styleUrls: ['./juridiction.component.css']
})
export class JuridictionComponent implements OnInit {
  test:number[] = [1,3,6];
  juridictions: any;
  juridiction: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'JUR';

  typeSelected: any;
  juriPereSelected: any;

  powers = ['Really Smart', 'Super Flexible',
            'Super Hot', 'Weather Changer'];
  juriPereForm : FormGroup;

  constructor(private generalService: GeneralService,private modalService: BsModalService,private fb : FormBuilder) { }

  ngOnInit() {
    this.onGetJuridictionsByPages();
    this.juriPereForm = this.fb.group({juriPereControl:['']})

    this.juriPereForm = new FormGroup({
      juriPereControl: new FormControl()
   });
  }

  onGetJuridictionsByPages(){
    this.generalService.getPagination(API_URL.JuridictionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.juridictions = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPagejuridiction(i:number){
    this.current_page = i;
    this.onGetJuridictionsByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchJuridiction();
  }

  SearchJuridiction(){
    this.generalService.getByKeyword(API_URL.JuridictionsSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.juridictions = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveJuridiction(form:any){
    this.generalService.saveResource(API_URL.JuridictionsUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetJuridictionsByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(i:any){
    this.update = 1;
    this.juridiction = i;
  }

  onUpdateJuridiction(values:any){
    this.generalService.updateResource(API_URL.JuridictionsUrl+"/"+this.juridiction.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetJuridictionsByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
