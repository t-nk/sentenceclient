import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { GeneralService } from '../services/general.service';
import {API_URL} from '../config/api.url.config';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  user: any
  groupe: any

  ttPersonnes : number = 0
  ttCondamnations:number = 0
  ttPeines:number = 0
  nbreExEnCours:number = 0
  nbreExEnd3Days:number = 0
  nbreExEnd:number = 0
  stock :any
  size: number = 20;
  current_page:number = 0;
  constructor(private authService: AuthService,private generalService: GeneralService) { }
 
  ngOnInit() {
    this.user = this.authService.user;
    this.groupe = this.user.groupe;
    console.log(this.groupe);
    //chargement nbre de personnes
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.ttPersonnes = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //chargement nbre de condamnations
    this.generalService.getPagination(API_URL.CondamnationsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.ttCondamnations = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //Chargement nbre de peines
    this.generalService.getPagination(API_URL.PeinesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.ttPeines = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //chargement nbre exécutions en cours
    this.generalService.getPagination(API_URL.ExecutionsUrl,this.current_page,this.size)
    .subscribe(data=>{
        let i:number = 0
        this.stock = data
        for(i=0;i<data.page.totalElements;i++){
          //nbre exécutions en cours
          if(this.stock._embedded.executions[i].statutExecution == 'encours'){
            this.nbreExEnCours += 1
          } 
          //nbre exécutions achevées
          if(this.stock._embedded.executions[i].statutExecution == 'purgee'){
            this.nbreExEnd += 1
          }
          //nbre exéécutioon dans 3 jours
          let dateFin = new Date(this.stock._embedded.executions[i].dateFinExecution);
          let dateActu = new Date();
          if(( dateFin.valueOf() - dateActu.valueOf() ) <= 3){
            this.nbreExEnd3Days =+ 1
          }
        }
    },error=>{
      console.log(error);
    })
  }

}
