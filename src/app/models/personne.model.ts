export class Personne {
    constructor(
      public nom: string = "",
      public prenoms: string = "",
      public sexe: string = "",
      public dateNaissance: Date,
      public lieuNaissance : any = null,
      public profession : string = "",
      public telephone : number = 0,
      public email : string = "",
      public nomPere : string = "",
      public nomMere : string = "",
      public nomMarital : string = "",
      public alias : string = "",
      public entiteMorale : string = "",
      public civilite : any = null,
      public situationMatrimoniale : any = null,
      public adresses : any = null,
    ){}
}

