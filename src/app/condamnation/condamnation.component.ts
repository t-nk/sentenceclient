import { Component, OnInit } from '@angular/core';
import {API_URL} from '../config/api.url.config';
import { GeneralService } from '../services/general.service';

@Component({
  selector: 'app-condamnation',
  templateUrl: './condamnation.component.html',
  styleUrls: ['./condamnation.component.css']
})
export class CondamnationComponent implements OnInit {

  condamnations: any;
  condamnation: any;
  personnes: any;
  affaires: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  update: number = 0;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
    this.onGetCondamnationByPages();
  }

  onGetCondamnationByPages(){
    this.generalService.getPagination(API_URL.CondamnationsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.condamnations = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageCondamnation(i:number){
    this.current_page = i;
    this.onGetCondamnationByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchCondamnation();
  }

  SearchCondamnation(){/*
    this.generalService.getByKeyword(API_URL.,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.adresses = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })*/
  }

  onEdit(c:any){
    this.update = 1;
    this.condamnation = c;
  }

  onUpdateCondamnation(values:any){
    this.generalService.updateResource(API_URL.CondamnationsUrl+"/"+this.condamnation.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetCondamnationByPages();
      },err=>{
        console.log(err);
      })
    }
  }
}
