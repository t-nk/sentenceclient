import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.css']
})
export class AdresseComponent implements OnInit {

  adresses: any;
  adresse: any;
  uniteAdministratives: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'ADR';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetAdresseByPages();

    this.generalService.getPagination(API_URL.UniteAdsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.uniteAdministratives = data;
    },error=>{
      console.log(error);
    })
  }

  onGetAdresseByPages(){
    this.generalService.getPagination(API_URL.AdressesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.adresses = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageAdresse(i:number){
    this.current_page = i;
    this.onGetAdresseByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchAdresse();
  }

  SearchAdresse(){
    this.generalService.getByKeyword(API_URL.AdressesSearchMaisonPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.adresses = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveAdresse(form:any){
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetAdresseByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(a:any){
    this.update = 1;
    this.adresse = a;
  }

  onUpdateAdresse(values:any){
    this.generalService.updateResource(API_URL.AdressesUrl+"/"+this.adresse.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(a:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(a._links.self.href)
      .subscribe(data=>{
        this.onGetAdresseByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
