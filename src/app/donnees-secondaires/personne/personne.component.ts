import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';
import { FormGroup,FormControl} from '@angular/forms'; 

@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

  personnes: any;
  personne: any;
  adresses: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  //nbreASelected: number = 0;
  sexes=['masculin','feminin'];
  PersonneForm:FormGroup;
  uniteAdministratives: any;
  totalElementsAdr:number = 0;
  totalElementsPer:number = 0;
  tableCodePer: string = 'PER';
  tableCodeAdr: string = 'ADR';
  stock: any;
  lieuNaissInter: any;

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetPersonneByPages();

    //Chargement unités administratives
    this.generalService.getPagination(API_URL.UniteAdsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.uniteAdministratives = data;
    },error=>{
      console.log(error);
    })
    //charger nombre adresses
    this.generalService.getPagination(API_URL.AdressesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsAdr = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //charger nombre personnes
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsPer = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //création champs formulaire 
    this.PersonneForm = new FormGroup({
      'code':new FormControl(0),
      'nom':new FormControl(null),
      'prenoms':new FormControl(null),
      'sexe':new FormControl(null),
      'dateNaissance':new FormControl(null),
      'lieuNaissance':new FormControl(null),
      'telephone':new FormControl(null),
      'email':new FormControl(null),
      'profession':new FormControl(null),
      'nomPere':new FormControl(null),
      'nomMere':new FormControl(null),
      'nomMarital':new FormControl(null,),
      'alias':new FormControl(null),
      'entiteMorale':new FormControl(null),
      'civilite':new FormControl(null),
      'situationMatrimoniale':new FormControl(null),
      'fonction':new FormControl(null),
      //'adresses':new FormArray([])
      'adresse':new FormControl(null)
    });
  }

  onGetPersonneByPages(){
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.personnes = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPagePersonne(i:number){
    this.current_page = i;
    this.onGetPersonneByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchPersonne();
  }

  SearchPersonne(){
    this.generalService.getByKeyword(API_URL.PersonnesSearchNomPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.personnes = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onEdit(p:any){
    this.update = 1;
    this.personne = p;
  }

  onGetLieuNaissance(form:any){
    this.lieuNaissInter = form
    this.PersonneForm.value.lieuNaissance = form
    //console.log(this.PersonneForm.value.lieuNaissance)
    this.modalRef.hide()
  }

  onGetAdresse(form:any){
    this.PersonneForm.value.adresse = form
    this.modalRef.hide()
  }

  onUpdatePersonne(values:any){
    //suppression lieu naissance
    this.generalService.getByKeyword(API_URL.AdressesSearchCodePagesUrl,this.personne.lieuNaissance.code, this.current_page,this.size)
    .subscribe(data=>{
      this.stock = data;
    },error=>{
      console.log(error);
    })
    console.log(this.stock)/*
    this.generalService.deleteResource(this.stock._links.self.href)
      .subscribe(data=>{
        this.totalElementsAdr -= 1;
      },err=>{
        console.log(err);
      })
    //suppression adresse
    this.generalService.getByKeyword(API_URL.AdressesSearchCodePagesUrl,this.personne.adresse.code, this.current_page,this.size)
    .subscribe(data=>{
      this.stock = data._embedded.adresses[0];
    },error=>{
      console.log(error);
    })
    this.generalService.deleteResource(this.stock._links.self.href)
      .subscribe(data=>{
        this.totalElementsAdr -= 1;
      },err=>{
        console.log(err);
      })
    //ajout nouveau lieu de naissance
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.PersonneForm.value.lieuNaissance,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //ajout nouvelle adresse
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.PersonneForm.value.adresse,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //modification de la personne
    this.generalService.updateResource(API_URL.PersonnesUrl+"/"+this.personne.code,this.PersonneForm.value)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    })*/
  }

  onDelete(p:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(p._links.self.href)
      .subscribe(data=>{
        this.onGetPersonneByPages();
      },err=>{
        console.log(err);
      })
    }}
  
  public openLieuNaissanceModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  public openAdresseModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }
}
