import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';
import { FormGroup,FormControl,Validators,FormArray} from '@angular/forms';

@Component({
  selector: 'app-affaire',
  templateUrl: './affaire.component.html',
  styleUrls: ['./affaire.component.css']
})
export class AffaireComponent implements OnInit {

  affaires: any;
  affaire: any;
  AffaireForm:FormGroup;
  AffaireUpdateForm:FormGroup;
  juridictions: any;
  chefpoursuites: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'AFF';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetAffairesByPages();
    //chargement juridictions
    this.generalService.getPagination(API_URL.JuridictionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.juridictions = data;
    },error=>{
      console.log(error);
    })
    //chargement chefPoursuites
    this.generalService.getPagination(API_URL.ChefPoursuitesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.chefpoursuites = data;
      console.log(this.chefpoursuites)
    },error=>{
      console.log(error);
    })

    this.AffaireForm = new FormGroup({
      'code':new FormControl(0),
      'numero':new FormControl(null),
      'dateDebutAffaire':new FormControl(null),
      'juridiction':new FormControl(null),
      'chefPoursuite':new FormControl(null),
    });
  }

  onGetAffairesByPages(){
    this.generalService.getPagination(API_URL.AffairesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.affaires = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageAffaire(i:number){
    this.current_page = i;
    this.onGetAffairesByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchAffaire();
  }

  SearchAffaire(){
    this.generalService.getByKeyword(API_URL.AffairesSearchNumeroPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.affaires = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveAffaire(){
    console.log(this.AffaireForm.value)
    this.generalService.saveResource(API_URL.AffairesUrl,this.tableCode,this.AffaireForm.value,this.totalElements)
    .subscribe(res=>{
      this.onGetAffairesByPages();
    },err=>{
      console.log(err);
    })
  }

  onGetChefPoursuite(form:any){
    this.AffaireForm.value.chefPoursuites.push(form.chefPoursuite)
    this.modalRef.hide()
  }

  onEdit(a:any){
    this.update = 1;
    this.affaire = a;
    this.AffaireUpdateForm = new FormGroup({
      'numero':new FormControl(this.affaire.numero),
      'dateDebutAffaire':new FormControl(this.affaire.dateDebutAffaire),
      'juridiction':new FormControl(this.affaire.juridiction),
      'chefPoursuite':new FormControl(this.affaire.chefPoursuite),
    });
  }

  onUpdateAffaire(){
    console.log(this.AffaireUpdateForm)
    this.generalService.updateResource(API_URL.AffairesUrl+"/"+this.affaire.code,this.AffaireUpdateForm.value)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
      this.onGetAffairesByPages();
      },err=>{
        console.log(err);
      }
    )
  }

  onDelete(a:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(a._links.self.href)
      .subscribe(data=>{
        this.onGetAffairesByPages(); 
        this.AffaireForm.reset;
      },err=>{
        console.log(err);
      })
    }
  }

  public openChefPoursuiteModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}