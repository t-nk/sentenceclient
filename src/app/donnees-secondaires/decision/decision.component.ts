import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';


@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.css']
})
export class DecisionComponent implements OnInit {

  decisions: any;
  decision: any;
  affaires: any;
  personnes: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'DEC';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetDecisionsByPages();

    this.generalService.getPagination(API_URL.AffairesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.affaires = data;
    },error=>{
      console.log(error);
    })
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.personnes = data;
    },error=>{
      console.log(error); 
    })
  }

  onGetDecisionsByPages(){
    this.generalService.getPagination(API_URL.DecisionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.decisions = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageDecision(i:number){
    this.current_page = i;
    this.onGetDecisionsByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchDecision;
  }

  SearchDecision(){
    this.generalService.getByKeyword(API_URL.DecisionsSearchNumeroPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.decisions = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveDecision(form:any){ 
    console.log(form)
    this.generalService.saveResource(API_URL.DecisionsUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetDecisionsByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(d:any){
    this.update = 1;
    this.decision = d;
  }

  onUpdateDecision(values:any){
    this.generalService.updateResource(API_URL.DecisionsUrl+"/"+this.decision.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(d:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(d._links.self.href)
      .subscribe(data=>{
        this.onGetDecisionsByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
