import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeineComponent } from './peine.component';

describe('PeineComponent', () => {
  let component: PeineComponent;
  let fixture: ComponentFixture<PeineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
