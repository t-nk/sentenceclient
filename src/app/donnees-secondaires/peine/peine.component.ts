import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-peine',
  templateUrl: './peine.component.html',
  styleUrls: ['./peine.component.css']
})
export class PeineComponent implements OnInit {

  peines: any;
  peine: any;
  condamnations: any;
  typePeines: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'PEI';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetPeinesByPages(); 

    this.generalService.getPagination(API_URL.TypesPeineUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.typePeines = data;
    },error=>{
      console.log(error);
    })
    this.generalService.getPagination(API_URL.CondamnationsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.condamnations = data;
    },error=>{
      console.log(error);
    })
  }

  onGetPeinesByPages(){
    this.generalService.getPagination(API_URL.PeinesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages = new Array<number>(this.totalPages);
      this.peines = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPagePeine(i:number){
    this.current_page = i;
    this.onGetPeinesByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchPeine();
  }

  SearchPeine(){/*
    this.generalService.getByKeyword(API_URL.TypesPeineSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.peines = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })*/
  }

  onSavePeine(form:any){
    form.duree = (form.ans*12)+form.mois;
    this.generalService.saveResource(API_URL.PeinesUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetPeinesByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(p:any){
    this.update = 1;
    this.peine = p;
  }

  onUpdatePeine(form:any){
    form.duree = (form.ans*12)+form.mois;
    this.generalService.updateResource(API_URL.PeinesUrl+"/"+this.peine.code,form)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
      this.onGetPeinesByPages();
    },err=>{
      console.log(err);
    })
  }

  onDelete(a:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(a._links.self.href)
      .subscribe(data=>{
        this.onGetPeinesByPages();
      },err=>{
        console.log(err);
      })
    }
  }
  
  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
