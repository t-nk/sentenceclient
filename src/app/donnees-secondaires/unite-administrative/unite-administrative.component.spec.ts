import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniteAdministrativeComponent } from './unite-administrative.component';

describe('UniteAdministrativeComponent', () => {
  let component: UniteAdministrativeComponent;
  let fixture: ComponentFixture<UniteAdministrativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniteAdministrativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniteAdministrativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
