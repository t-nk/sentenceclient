import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';

@Component({
  selector: 'app-unite-administrative',
  templateUrl: './unite-administrative.component.html',
  styleUrls: ['./unite-administrative.component.css']
})
export class UniteAdministrativeComponent implements OnInit {

  uniteAdministratives: any;
  uniteAdministrative: any;
  payses: any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'U-A';

  uaSelected: any;
  paysSelected: any;

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetUniteAdsByPages();

    this.generalService.getPagination(API_URL.PaysUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.payses = data;
    },error=>{
      console.log(error);
    })
  }

  onGetUniteAdsByPages(){
    this.generalService.getPagination(API_URL.UniteAdsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.uniteAdministratives = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageUniteAd(i:number){
    this.current_page = i;
    this.onGetUniteAdsByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchUniteAd();
  }

  SearchUniteAd(){
    this.generalService.getByKeyword(API_URL.UniteAdsSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.uniteAdministratives = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })
  }

  onSaveUniteAd(form:any){
    this.generalService.saveResource(API_URL.UniteAdsUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      console.log(res);
      this.modalRef.hide();
      this.onGetUniteAdsByPages();
    },err=>{
      console.log(err);
      this.modalRef.hide();
      this.onGetUniteAdsByPages();
    })
  }

  onEdit(u:any){
    this.update = 1;
    this.uniteAdministrative = u;
  }

  onUpdateUniteAd(values:any){
    this.generalService.updateResource(API_URL.UniteAdsUrl+"/"+this.uniteAdministrative.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetUniteAdsByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}