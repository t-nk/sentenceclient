import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChefPoursuiteComponent } from './chef-poursuite.component';

describe('ChefPoursuiteComponent', () => {
  let component: ChefPoursuiteComponent;
  let fixture: ComponentFixture<ChefPoursuiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChefPoursuiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChefPoursuiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
