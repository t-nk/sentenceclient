import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../../config/api.url.config';
import { GeneralService } from '../../services/general.service';
import { FormGroup,FormControl,Validators,FormArray} from '@angular/forms';

@Component({
  selector: 'app-chef-poursuite',
  templateUrl: './chef-poursuite.component.html',
  styleUrls: ['./chef-poursuite.component.css']
})
export class ChefPoursuiteComponent implements OnInit {

  chefPoursuites: any;
  chefPoursuite: any;
  ChefPoursuiteForm:FormGroup;
  ChefPoursuiteUpdateForm:FormGroup;
  ajout: number = 0;
  personnes: any;
  adresses: any;
  qualites: any
  infractions: any;
  uniteAdministratives: any;
  totalElementsAdr: number = 0;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElementsCp:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCodeAdr: string = 'ADR';
  tableCodeCp: string = 'C-PR';
  lieuInfractionSelected: any;
  lieuInfractionModifSelected: any;
  stock:any 
  inter :any

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit(){
    this.onGetChefPoursuiteByPages();
    //chargement des procureurs
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.personnes = data;
    },error=>{
      console.log(error);
    })
    //charger nombre adresses
    this.generalService.getPagination(API_URL.AdressesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsAdr = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    this.generalService.getPagination(API_URL.QualitesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.qualites = data;
    },error=>{
      console.log(error);
    })
    this.generalService.getPagination(API_URL.InfractionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.infractions = data;
    },error=>{
      console.log(error);
    })
    this.generalService.getPagination(API_URL.UniteAdsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.uniteAdministratives = data
    },error=>{
      console.log(error);
    })
    
    this.ChefPoursuiteForm = new FormGroup({
      'code':new FormControl(0),
      'dateInfraction':new FormControl(null),
      'details':new FormControl(null),
      'qualite':new FormControl(null),
      'infraction':new FormControl(null),
      'procureur':new FormControl(null),
      'lieuInfraction':new FormControl(null),
    });
    
  }
  
  onGetChefPoursuiteByPages(){
    this.generalService.getPagination(API_URL.ChefPoursuitesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElementsCp = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.chefPoursuites = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageChefPoursuite(i:number){
    this.current_page = i;
    this.onGetChefPoursuiteByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchChefPoursuite();
  }

  SearchChefPoursuite(){/*
    this.generalService.getByKeyword(API_URL.ChefPoursuitesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.chefPoursuites = data;
      this.update = 0;
    },error=>{
      console.log(error);
    }) */
  }

  onGetLieuInfraction(form:any){
    this.lieuInfractionSelected = form
    this.ChefPoursuiteForm.value.lieuInfraction = this.lieuInfractionSelected
    this.modalRef.hide()
  }
  
  onGetInfraction(form:any){
    this.inter = form.infraction
    this.ChefPoursuiteForm.value.infraction = this.inter
    this.modalRef.hide()
  }

  onSaveChefPoursuite(){
    //enrégistrement du lieu de l'infraction
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.ChefPoursuiteForm.value.lieuInfraction,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //enrégistrement du chef de poursuite
    this.generalService.saveResource(API_URL.ChefPoursuitesUrl,this.tableCodeCp,this.ChefPoursuiteForm.value,this.totalElementsCp)
    .subscribe(res=>{
      this.onGetChefPoursuiteByPages();
    },err=>{
      console.log(err);
    })
    this.ChefPoursuiteForm.reset;
  }

  onEdit(cp:any){
    this.update = 1;
    this.chefPoursuite = cp;
    
    this.ChefPoursuiteUpdateForm = new FormGroup({
      'dateInfraction':new FormControl(this.chefPoursuite.dateInfraction),
      'qualite':new FormControl(null),
      'details':new FormControl(this.chefPoursuite.details),
      'infraction':new FormControl(null),
      'procureur':new FormControl(null),
      'lieuInfraction':new FormControl(null),
    });
  }

  onGetLieuInfractionModif(form:any){
    this.lieuInfractionModifSelected = form
    this.ChefPoursuiteUpdateForm.value.lieuInfraction = this.lieuInfractionModifSelected
    this.modalRef.hide()
  }

  onUpdateChefPoursuite(values:any){
    //suppression lieu infraction
    this.generalService.getByKeyword(API_URL.AdressesSearchCodePagesUrl,this.chefPoursuite.lieuInfraction.code, this.current_page,this.size)
    .subscribe(data=>{
      this.stock = data._embedded.adresses[0];
    },error=>{
      console.log(error);
    })
    this.generalService.deleteResource(this.stock._links.self.href)
      .subscribe(data=>{
        this.totalElementsAdr -= 1;
      },err=>{
        console.log(err);
      })
    //ajout nouveau lieu infraction
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.ChefPoursuiteForm.value.lieuInfraction.value.lieuNaissance,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //modifications chef de poursuite
    this.generalService.updateResource(API_URL.ChefPoursuitesUrl+"/"+this.chefPoursuite.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    })
    this.ChefPoursuiteUpdateForm.reset;
    this.onGetChefPoursuiteByPages()
  }

  onDelete(c:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(c._links.self.href)
      .subscribe(data=>{
        this.onGetChefPoursuiteByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openLieuInfractionModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  public openLieuInfractionModifModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  public openInfractionsModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

}
