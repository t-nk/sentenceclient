import { Component, OnInit,TemplateRef } from '@angular/core';
import { GeneralService } from '../../services/general.service';
import { FormGroup,FormControl,Validators,FormArray, FormArrayName} from '@angular/forms'; 
import {API_URL} from '../../config/api.url.config';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-personne',
  templateUrl: './new-personne.component.html',
  styleUrls: ['./new-personne.component.css']
})
export class NewPersonneComponent implements OnInit {
  i : number = 0;
  modalRef: BsModalRef;
  sexes=['masculin','feminin'];
  PersonneForm:FormGroup;
  lieuNaissanceSelected: any;
  uniteAdministratives: any;
  totalElementsAdr:number = 0;
  totalElementsPer:number = 0;
  size: number = 20;
  current_page:number = 0;
  tableCodePer: string = 'PER';
  tableCodeAdr: string = 'ADR';
  lieuNaissInter:any;

  constructor(private generalService: GeneralService,
    private modalService: BsModalService,private router: Router) { }

  ngOnInit(){
    //Chargement unités administratives
    this.generalService.getPagination(API_URL.UniteAdsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.uniteAdministratives = data;
    },error=>{
      console.log(error);
    })
    //charger nombre adresses
    this.generalService.getPagination(API_URL.AdressesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsAdr = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //charger nombre personnes
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsPer = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //création champs formulaire 
    this.PersonneForm = new FormGroup({
      'code':new FormControl(0),
      'nom':new FormControl(null),
      'prenoms':new FormControl(null),
      'sexe':new FormControl(null),
      'dateNaissance':new FormControl(null),
      'lieuNaissance':new FormControl(null),
      'email':new FormControl(null),
      'profession':new FormControl(null),
      'nomPere':new FormControl(null),
      'nomMere':new FormControl(null),
      'nomMarital':new FormControl(null,),
      'alias':new FormControl(null),
      'entiteMorale':new FormControl(null),
      'civilite':new FormControl(null),
      'situationMatrimoniale':new FormControl(null),
      'fonction':new FormControl(null),
      //'adresses':new FormArray([])
      'adresse':new FormControl(null)
    });
    //this.adresseData = new FormData();
  }

  onGetLieuNaissance(form:any){
    this.lieuNaissInter = form
    this.PersonneForm.value.lieuNaissance = form
    this.modalRef.hide()
  }

  onGetAdresse(form:any){
    //this.adresseData.append(this.i,form)
    //this.i++
    this.PersonneForm.value.adresse = form
    this.modalRef.hide()
  }

  onSubmit(){
    this.PersonneForm.value.lieuNaissance = this.lieuNaissInter
    console.log(this.PersonneForm.value)
    //this.PersonneForm.value.adresses = this.InterForm.value.adresses
    
    
    //enrégistrement du lieu de naissance
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.PersonneForm.value.lieuNaissance,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //enrégistrement des adresses
    //for(this.i = 0;this.i < this.PersonneForm.value.adresses.length;this.i ++){
    this.generalService.saveResource(API_URL.AdressesUrl,this.tableCodeAdr,this.PersonneForm.value.adresse,this.totalElementsAdr)
    .subscribe(res=>{
      this.totalElementsAdr += 1;
    },err=>{
      console.log(err);
    })
    //}
    //enrégistrement de la personne
    this.generalService.saveResource(API_URL.PersonnesUrl,this.tableCodePer,this.PersonneForm.value,this.totalElementsPer)
    .subscribe(res=>{
      this.router.navigate(['personnes']);
    },err=>{
      console.log(err);
    })

  }

  public openLieuNaissanceModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  public openAdresseModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }
}