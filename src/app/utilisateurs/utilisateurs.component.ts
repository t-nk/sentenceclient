import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { GeneralService } from '../services/general.service';
import { Router } from '@angular/router';
import {API_URL} from '../config/api.url.config';

@Component({
  selector: 'app-utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: ['./utilisateurs.component.css']
})

export class UtilisateursComponent implements OnInit {
  //utilisateur
  utilisateurs: any;
  utilisateur: any;
  u_size: number = 20;
  u_current_page:number = 0;
  u_totalPages:number = 0;
  u_totalElements:number = 0;
  u_pages:Array<number>;
  u_current_keyword:string = '';
  u_modalRef: BsModalRef;
  u_update: number = 0;
  u_tableCode: string = 'UTI';
  //groupes
  groupes: any;
  groupe: any;
  g_size: number = 20;
  g_current_page:number = 0;
  g_totalPages:number = 0;
  g_totalElements:number = 0;
  g_pages:Array<number>;
  g_current_keyword:string = '';
  g_modalRef: BsModalRef;
  g_update: number = 0;
  g_tableCode: string = 'GRP';

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.u_onGetUtilisateursByPages();
    this.g_onGetGroupesByPages();
  }

  u_onGetUtilisateursByPages(){
    this.generalService.getPagination(API_URL.UtilisateursUrl,this.u_current_page,this.u_size)
    .subscribe(data=>{
      this.u_totalPages = data.page.totalPages;
      this.u_pages=new Array<number>(this.u_totalPages);
      this.utilisateurs = data;
      this.u_update = 0
    },error=>{
      console.log(error);
    })
  }

  g_onGetGroupesByPages(){
    this.generalService.getPagination(API_URL.GroupesUrl,this.g_current_page,this.g_size)
    .subscribe(data=>{
      this.g_totalPages = data.page.totalPages;
      this.g_pages=new Array<number>(this.g_totalPages);
      this.groupes = data;
      this.g_update = 0
    },error=>{
      console.log(error);
    })
  }

  u_onPageUtilisateur(i:number){
    this.u_current_page = i;
    this.u_onGetUtilisateursByPages();
  }

  g_onPageGroupe(i:number){
    this.g_current_page = i;
    this.g_onGetGroupesByPages;
  }

  u_onSearch(form:any){
    this.u_current_page=0;
    this.u_current_keyword= form.keyword;
    this.u_SearchUtilisateur();
  }

  u_SearchUtilisateur(){
    this.generalService.getByKeyword(API_URL.UtilisateursSearchByNomPagesUrl,this.u_current_keyword, this.u_current_page,this.u_size)
    .subscribe(data=>{ 
      this.u_totalPages = data.page.totalPages;
      this.u_pages = new Array<number>(this.u_totalPages);
      this.utilisateurs = data; 
    },error=>{
      console.log(error); 
    })
  }

  g_onSearch(form:any){
    this.g_current_page=0;
    this.g_current_keyword= form.keyword;
    this.g_SearchGroupe();
  }

  g_SearchGroupe(){
    this.generalService.getByKeyword(API_URL.GroupesSearchPagesUrl,this.g_current_keyword, this.g_current_page,this.g_size)
    .subscribe(data=>{
      this.g_totalPages = data.page.totalPages;
      this.g_pages = new Array<number>(this.g_totalPages);
      this.groupes = data;
    },error=>{
      console.log(error);
    })
  }

  u_onSaveUtilisateur(form:any){
    if(form.passe != form.passeConf){
      alert("Mot de passe incorrecte")
    }else{
      this.generalService.saveResource(API_URL.UtilisateursUrl,this.u_tableCode,form,this.u_totalElements)
      .subscribe(res=>{
        this.u_modalRef.hide();
        this.u_onGetUtilisateursByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  g_onSaveGroupe(form:any){ 
    //console.log(form)
    this.generalService.saveResource(API_URL.GroupesUrl,this.g_tableCode,form,this.g_totalElements)
    .subscribe(res=>{
      this.g_modalRef.hide();
      this.g_onGetGroupesByPages();
    },err=>{
      console.log(err);
    })
  }

  u_onEdit(u:any){
    this.u_update = 1;
    this.utilisateur = u;
  }

  u_onUpdateUtilisateur(values:any){
    this.generalService.updateResource(API_URL.GroupesUrl+"/"+this.utilisateur.login,values)
    .subscribe(data=>{
      this.u_update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    })
  }

  g_onEdit(g:any){
    this.g_update = 1;
    this.groupe = g;
  }

  g_onUpdateGroupe(values:any){
    this.generalService.updateResource(API_URL.GroupesUrl+"/"+this.groupe.code,values)
    .subscribe(data=>{
      this.g_update = 0;
      alert("Mise à jour effectuée avec succès");
    },err=>{
      console.log(err);
    }
    )
  }

  u_onDelete(u:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(u._links.self.href)
      .subscribe(data=>{
        this.u_onGetUtilisateursByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  g_onDelete(g:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(g._links.self.href)
      .subscribe(data=>{
        this.g_onGetGroupesByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public u_openModal(template: TemplateRef<any>) {
    this.u_modalRef = this.modalService.show(template);
  }

  public g_openModal(template: TemplateRef<any>) {
    this.g_modalRef = this.modalService.show(template);
  }
}
