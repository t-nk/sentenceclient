import { Component, OnInit } from '@angular/core';
import { GeneralService } from '../services/general.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import {API_URL} from '../config/api.url.config';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  
  user:any = null;
  constructor(private generalService: GeneralService,private authService : AuthService,
    private router: Router) { }

  ngOnInit() {
  }

  onConnect(form:any){

    this.generalService.getByKeyword(API_URL.UtilisateursSearchByLoginPagesUrl,form.login,0,20)
    .subscribe(data=>{
      this.user = data._embedded.utilisateurs[0];
      if(this.user.passe == form.passe){
        this.authService.isAuth = true
        this.authService.user = this.user
        alert("Connexion effectuée avec succès");
        this.router.navigate(['accueil']);
      }else{
        this.user = null;
        alert("Connexion échouée");
      }
    },error=>{
      console.log(error);
    })
    /*
    if(this.authService.signIn(form)){
      alert("Connexion effectuée avec succès");
      this.router.navigate(['accueil']); 
    }else{
      alert("Connexion échouée");
    }*/
  }
}
