import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import {API_URL} from '../config/api.url.config';
import { GeneralService } from '../services/general.service';

@Component({
  selector: 'app-execution',
  templateUrl: './execution.component.html',
  styleUrls: ['./execution.component.css']
})
export class ExecutionComponent implements OnInit {

  executions: any;
  execution: any;
  peines: any;
  maisonArrets:any;
  size: number = 20;
  current_page:number = 0;
  totalPages:number = 0;
  totalElements:number = 0;
  pages:Array<number>;
  current_keyword:string = '';
  modalRef: BsModalRef;
  update: number = 0;
  tableCode: string = 'EXE';
  stock:any;

  constructor(private generalService: GeneralService,private modalService: BsModalService) { }

  ngOnInit() {
    this.onGetExecutionByPages();
    //chargement des peines
    this.generalService.getPagination(API_URL.PeinesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.peines = data;
    },error=>{
      console.log(error);
    })
    //chargement des maisons d'arrêts
    this.generalService.getPagination(API_URL.MaisonArretUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.maisonArrets = data;
    },error=>{
      console.log(error);
    })
  }

  onGetExecutionByPages(){
    this.generalService.getPagination(API_URL.ExecutionsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.totalElements = data.page.totalElements;
      this.pages=new Array<number>(this.totalPages);
      this.executions = data;
      this.update = 0
    },error=>{
      console.log(error);
    })
  }

  onPageExecution(i:number){
    this.current_page = i;
    this.onGetExecutionByPages();
  }

  onSearch(form:any){
    this.current_page=0;
    this.current_keyword= form.keyword;
    this.SearchExecution();
  }

  SearchExecution(){/*
    this.generalService.getByKeyword(API_URL.AdressesSearchPagesUrl,this.current_keyword, this.current_page,this.size)
    .subscribe(data=>{
      this.totalPages = data.page.totalPages;
      this.pages = new Array<number>(this.totalPages);
      this.executions = data;
      this.update = 0;
    },error=>{
      console.log(error);
    })*/
  }

  onSaveExecution(form:any){
    let dateString = form.dateExecution
    let newDate = new Date(dateString);
    //this.stock = newDate.getMonth()+1
    form.dateFinExecution = newDate.setMonth(newDate.getMonth() + form.peine.duree) 
    this.generalService.saveResource(API_URL.ExecutionsUrl,this.tableCode,form,this.totalElements)
    .subscribe(res=>{
      this.modalRef.hide();
      this.onGetExecutionByPages();
    },err=>{
      console.log(err);
    })
  }

  onEdit(e:any){
    this.update = 1;
    this.execution = e;
  }

  onUpdateExecution(values:any){
    this.generalService.updateResource(API_URL.ExecutionsUrl+"/"+this.execution.code,values)
    .subscribe(data=>{
      this.update = 0;
      alert("Mise à jour effectuée avec succès");
      this.onGetExecutionByPages()
    },err=>{
      console.log(err);
    }
    )
  }

  onDelete(a:any){
    let conf = confirm("Etes vous sûre ?");
    if(conf){
      this.generalService.deleteResource(a._links.self.href)
      .subscribe(data=>{
        this.onGetExecutionByPages();
      },err=>{
        console.log(err);
      })
    }
  }

  public openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
