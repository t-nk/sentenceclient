export const API_URL = {
    //groupes
    GroupesUrl:"http://localhost:8080/groupes",
    GroupesSearchPagesUrl:"http://localhost:8080/groupes/search/byLibele",
    //infraction
    InfractionsUrl:"http://localhost:8080/infractions",
    InfractionsSearchPagesUrl:"http://localhost:8080/infractions/search/byLibele",
    //juridictions
    JuridictionsUrl:"http://localhost:8080/juridictions",
    JuridictionsSearchPagesUrl:"http://localhost:8080/juridictions/search/byDenomination",
    //pays
    PaysUrl:"http://localhost:8080/payses",
    PaysSearchPagesUrl:"http://localhost:8080/payses/search/byNom",
    //utilisateur
    UtilisateursUrl:"http://localhost:8080/utilisateurs",
    UtilisateursSearchByNomPagesUrl:"http://localhost:8080/utilisateurs/search/byNom",
    UtilisateursSearchByLoginPagesUrl:"http://localhost:8080/utilisateurs/search/byLogin",
    //qualités 
    QualitesUrl:"http://localhost:8080/qualites",
    QualitesSearchPagesUrl:"http://localhost:8080/qualites/search/byLibele",
    //typespeines
    TypesPeineUrl:"http://localhost:8080/typePeines",
    TypesPeineSearchPagesUrl:"http://localhost:8080/typePeines/search/byLibele",
    //maisons d'arrêt 
    MaisonArretUrl:"http://localhost:8080/maisonArrets",
    MaisonArretSearchPagesUrl:"http://localhost:8080/maisonArrets/search/byLibele",

    //uniteadministratives
    UniteAdsUrl:"http://localhost:8080/uniteAdministratives",
    //UniteAdsSaveUrl:"http://localhost:8080/saveUniteAd",
    UniteAdsSearchPagesUrl:"http://localhost:8080/uniteAdministratives/search/byDenomination",
    //adresses
    AdressesUrl:"http://localhost:8080/adresses",
    AdressesSearchMaisonPagesUrl:"http://localhost:8080/adresses/search/byMaison",
    AdressesSearchCodePagesUrl:"http://localhost:8080/adresses/search/byCode",
    //personnes
    PersonnesUrl:"http://localhost:8080/personnes",
    PersonnesSearchNomPagesUrl:"http://localhost:8080/personnes/search/byNom",
    //chefpoursuites 
    ChefPoursuitesUrl:"http://localhost:8080/chefPoursuites",
    //affaires 
    AffairesUrl:"http://localhost:8080/affaires",
    AffairesSearchNumeroPagesUrl:"http://localhost:8080/affaires/search/byNumero ",
    //decisions 
    DecisionsUrl:"http://localhost:8080/decisions",
    DecisionsSearchNumeroPagesUrl:"http://localhost:8080/decisions/search/byNumero ",
    //peines
    PeinesUrl:"http://localhost:8080/peines",
    //condamnations
    CondamnationsUrl:"http://localhost:8080/condamnations",
    //exécutions
    ExecutionsUrl:"http://localhost:8080/executions",
    
};