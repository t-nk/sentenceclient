import { Component, OnInit } from '@angular/core';
import {AuthService} from '../services/auth.service'
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  groupe : any ;
   
  constructor( private authService: AuthService, 
    private router: Router,) { }

  ngOnInit() {
    this.groupe = this.authService.user.groupe
  }

  logout(){
    this.authService.isAuth = false;
    this.authService.user = null;
    this.router.navigate(['connexion'])
  }
}
