import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {API_URL} from '../config/api.url.config';

export class GeneralService {
    date: Date;
    tierces: number;
    seconds: number;
    hours: number;
    minutes: number;
    year: number;
    monthIndex: number;
    day: number;

  constructor(private httpClient: HttpClient){}

  get(url:string):Observable<any>{
    return this.httpClient.get(url);
  }

  getPagination(url:string,page:number,size:number):Observable<any>{
    return this.httpClient.get(url+"?page="+page+"&size="+size);
  }

  getByKeyword(url:string,key:string, page:number, size:number):Observable<any>{
    return this.httpClient.get(url+"?key="+key+"&page="+page+"&size="+size);
  }

  deleteResource(url:string){
    return this.httpClient.delete(url);
  }

  saveResource(url:string,tableCode:string,data: any,totalElements:number){
    this.date = new Date();
    this.day = this.date.getDate();
    this.monthIndex = this.date.getMonth()+1;
    this.year = this.date.getFullYear();
    this.minutes = this.date.getMinutes();
    this.hours = this.date.getHours();
    this.seconds = this.date.getSeconds();
    this.tierces = this.date.getMilliseconds();
    data.code = tableCode+"."+this.year+"."+this.monthIndex+"."+this.day+"."+this.hours+"."+this.minutes+"."+this.seconds+"."+this.tierces+"."+(totalElements+1);
    console.log(data)
    return this.httpClient.post(url,data);
  }
  
  getResource(url:string,code:String){
    return this.httpClient.get(url+"/"+code);
  }

  updateResource(url,data){
    return this.httpClient.put(url,data);
  }
}