import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../app/services/auth.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'sentence';
  constructor(private router: Router,private authService: AuthService) { }
  
  ngOnInit() {
      this.router.navigate(['accueil']);
  }
}
