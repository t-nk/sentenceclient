import { Component, OnInit,TemplateRef } from '@angular/core';
import {API_URL} from '../config/api.url.config';
import { GeneralService } from '../services/general.service';
import { Router } from '@angular/router';
import { FormGroup,FormControl,FormArray} from '@angular/forms'; 
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'

@Component({
  selector: 'app-new-condamnation',
  templateUrl: './new-condamnation.component.html',
  styleUrls: ['./new-condamnation.component.css']
})
export class NewCondamnationComponent implements OnInit {
  CondaForm:FormGroup;  
  PeinesTable:FormGroup;
  personnes: any;
  affaires: any;
  typePeines: any;
  size: number = 20;
  current_page:number = 0;
  tableCodeC: string = 'CON';
  tableCodeP: string = 'PEI';
  totalElementsC:number = 0;
  totalElementsPeines:number = 0;
  i:number = 0;
  modalRef: BsModalRef;

  constructor(private generalService: GeneralService,
    private modalService: BsModalService,private router: Router) { }

  ngOnInit() {
    //chargement nombre de condamnations
    this.generalService.getPagination(API_URL.CondamnationsUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsC = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //chargement personnes
    this.generalService.getPagination(API_URL.PersonnesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.personnes = data;
    },error=>{
      console.log(error);
    })
    //chargement affaires
    this.generalService.getPagination(API_URL.AffairesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.affaires = data;
    },error=>{
      console.log(error);
    })
    //chargement des types de peines 
    this.generalService.getPagination(API_URL.TypesPeineUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.typePeines = data;
    },error=>{
      console.log(error);
    })
    //charger nombre peines
    this.generalService.getPagination(API_URL.PeinesUrl,this.current_page,this.size)
    .subscribe(data=>{
      this.totalElementsPeines = data.page.totalElements;
    },error=>{
      console.log(error);
    })
    //formulaire de données
    this.CondaForm = new FormGroup({
      'code':new FormControl(0),
      'dateSignificationDecision':new FormControl(null),
      'natureSignificationDecision':new FormControl(null),
      'affaire':new FormControl(null),
      'condamne':new FormControl(null),
      'beneficiaire':new FormControl(null)
    });
    //tableau des peines
    this.PeinesTable = new FormGroup({
      'peines':new FormArray([])
    }); 
  }

  public openPeineModal(template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }

  onGetPeine(form:any){
    form.duree = (form.ans*12)+form.mois;
    form.condamnation = this.CondaForm.value
    this.PeinesTable.value.peines.push(form)

    //this.PeineForm.value.condamnation = this.CondaForm.value
    //console.log(this.PeineForm.value)
    //console.log(this.PeineTable.value)
    console.log(this.PeinesTable)
    this.modalRef.hide() 
  }

  onSave(){
    //enrégistrement de la condamantion
    this.generalService.saveResource(API_URL.CondamnationsUrl,this.tableCodeC,this.CondaForm.value,this.totalElementsC)
    .subscribe(res=>{
    },err=>{
      console.log(err);
    }) 
    //enrégistrement des peines
    if(this.PeinesTable.value != null){
      for(this.i = 0;this.i < this.PeinesTable.value.peines.length;this.i ++){
        this.generalService.saveResource(API_URL.PeinesUrl,this.tableCodeP,this.PeinesTable.value.peines[this.i],this.totalElementsPeines)
        .subscribe(res=>{
          this.totalElementsPeines += 1;
        },err=>{
          console.log(err);
        })
      }
    }
    //retour aux condmantions
    this.router.navigate(['condamnation']);
  }
  
}
