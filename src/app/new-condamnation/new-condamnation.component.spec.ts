import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewCondamnationComponent } from './new-condamnation.component';

describe('NewCondamnationComponent', () => {
  let component: NewCondamnationComponent;
  let fixture: ComponentFixture<NewCondamnationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewCondamnationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewCondamnationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
