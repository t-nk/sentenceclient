import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Routes, RouterModule  } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
//services
import { GeneralService } from './services/general.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth-guard.service';
//components
import { AppComponent } from './app.component';
import { ConnexionComponent } from './connexion/connexion.component';
import { UtilisateursComponent } from './utilisateurs/utilisateurs.component';
import { TypePeineComponent } from './donnees-primaires/type-peine/type-peine.component';
import { PaysComponent } from './donnees-primaires/pays/pays.component';
import { InfractionComponent } from './donnees-primaires/infraction/infraction.component';
import { JuridictionComponent } from './donnees-primaires/juridiction/juridiction.component';
import { QualiteComponent } from './donnees-primaires/qualite/qualite.component';
import { UniteAdministrativeComponent } from './donnees-secondaires/unite-administrative/unite-administrative.component';
import { AdresseComponent } from './donnees-secondaires/adresse/adresse.component';
import { PersonneComponent } from './donnees-secondaires/personne/personne.component';
import { AffaireComponent } from './donnees-secondaires/affaire/affaire.component';
import { DecisionComponent } from './donnees-secondaires/decision/decision.component';
import { PeineComponent } from './donnees-secondaires/peine/peine.component';
import { ChefPoursuiteComponent } from './donnees-secondaires/chef-poursuite/chef-poursuite.component';
import { NewPersonneComponent } from './donnees-secondaires/new-personne/new-personne.component';
import { AccueilComponent } from './accueil/accueil.component';
import { CondamnationComponent } from './condamnation/condamnation.component';
import { NewCondamnationComponent } from './new-condamnation/new-condamnation.component';
import { UtilisateurComponent } from './utilisateur/utilisateur.component';
import { ExecutionComponent } from './execution/execution.component';
import { MaisonArretComponent } from './donnees-primaires/maison-arret/maison-arret.component';

const appRoutes: Routes = [
  { path: 'accueil',canActivate: [AuthGuard], component:AccueilComponent },
  { path: 'connexion', component:ConnexionComponent },

  { path: 'infractions',canActivate: [AuthGuard], component: InfractionComponent},
  { path: 'juridictions',canActivate: [AuthGuard], component: JuridictionComponent},
  { path: 'pays',canActivate: [AuthGuard], component: PaysComponent},
  { path: 'utilisateurs',canActivate: [AuthGuard], component: UtilisateursComponent},
  { path: 'utilisateur',canActivate: [AuthGuard], component: UtilisateurComponent},
  { path: 'qualites',canActivate: [AuthGuard], component: QualiteComponent},
  { path: 'typePeines',canActivate: [AuthGuard], component: TypePeineComponent},
  { path: 'maisonArrets',canActivate: [AuthGuard], component: MaisonArretComponent},

  { path: 'uniteAds',canActivate: [AuthGuard], component: UniteAdministrativeComponent},
  { path: 'adresses',canActivate: [AuthGuard], component: AdresseComponent},
  { path: 'personnes',canActivate: [AuthGuard], component: PersonneComponent},
  { path: 'new-personne',canActivate: [AuthGuard], component: NewPersonneComponent},
  { path: 'chefPoursuites',canActivate: [AuthGuard], component: ChefPoursuiteComponent},
  { path: 'affaires',canActivate: [AuthGuard], component: AffaireComponent},
  { path: 'decisions',canActivate: [AuthGuard], component: DecisionComponent},
  { path: 'peines',canActivate: [AuthGuard], component: PeineComponent},
  { path: 'executions',canActivate: [AuthGuard], component: ExecutionComponent},
  { path: 'condamnations',canActivate: [AuthGuard], component: CondamnationComponent},
  { path: 'new-condamnation',canActivate: [AuthGuard], component: NewCondamnationComponent},
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ConnexionComponent,
    InfractionComponent,
    JuridictionComponent,
    PaysComponent,
    QualiteComponent,
    TypePeineComponent,
    UniteAdministrativeComponent,
    UtilisateursComponent,
    AdresseComponent,
    PersonneComponent,
    AffaireComponent,
    DecisionComponent,
    PeineComponent,
    ChefPoursuiteComponent,
    NewPersonneComponent,
    AccueilComponent,
    CondamnationComponent,
    NewCondamnationComponent,
    UtilisateurComponent,
    ExecutionComponent,
    MaisonArretComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [GeneralService,AuthService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
